<?php
/**
 * Relynt API v2.0 demo script
 * Author: Volodymyr Tsumanchuk (Relynt s.r.o.)
 * https://relyntv2rc.docs.apiary.io - API documentation
 */

include '../src/RelyntApi.php';

$api_url = 'http://relynt/'; // please set your Relynt URL

$admin_login = "ADMIN_LOGIN"; // Relynt administrator login
$admin_password = "ADMIN_PASSWORD"; // Relynt administrator password

$api = new RelyntAPI($api_url);
$api->setVersion(RelyntApi::API_VERSION_2);

$isAuthorized = $api->login([
    'auth_type' => RelyntApi::AUTH_TYPE_ADMIN,
    'login' => $admin_login,
    'password' => $admin_password,
]);

if (!$isAuthorized) {
    exit("Authorization failed!\n");
}

print "<pre>";

print "Authorization info: " . var_export($api->getAuthData(), 1) . "\n";

$locationsApiUrl = "admin/administration/locations";

print "List locations\n";
$result = $api->api_call_get($locationsApiUrl);
print "Result: ";
if ($result) {
    print "Ok!\n";
    print_r($api->response);
} else {
    print "Fail! Error code: $api->response_code\n";
    print_r($api->response);
}
print "\n-------------------------------------------------\n";
