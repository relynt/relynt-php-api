<?php
/**
 * Relynt API v2.0 demo script
 * Author: Volodymyr Tsumanchuk (Relynt s.r.o.)
 * https://relyntv2rc.docs.apiary.io - API documentation
 */

include '../src/RelyntApi.php';

$api_url = 'http://relynt/'; // please set your Relynt URL

$session_id= "SESSION_ID"; // Relynt session id

$api = new RelyntAPI($api_url);
$api->setVersion(RelyntApi::API_VERSION_2);

$isAuthorized = $api->login([
    'auth_type' => RelyntApi::AUTH_TYPE_SESSION,
    'session_id' => $session_id,
]);

if (!$isAuthorized) {
    exit("Authorization failed!\n");
}

print "Authorization info: " . var_export($api->getAuthData(), 1) . "\n";

$customersApiUrl = "admin/customers/customer";

print "Get customers\n";
$result = $api->api_call_get($customersApiUrl);
print "Result: ";
if ($result) {
    print "Ok!\n";
    print_r($api->response);
} else {
    print "Fail! Error code: $api->response_code\n";
    print_r($api->response);
}
print "\n-------------------------------------------------\n";
