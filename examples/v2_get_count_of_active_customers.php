<?php
/**
 * Relynt API v2.0 demo script
 * Author: Volodymyr Tsumanchuk (Relynt s.r.o.)
 * https://relyntv2rc.docs.apiary.io - API documentation
 */

include '../src/RelyntApi.php';

$api_url = 'http://relynt/'; // please set your Relynt URL

$admin_login = "ADMIN_LOGIN"; // Relynt administrator login
$admin_password = "ADMIN_PASSWORD"; // Relynt administrator password

$api = new RelyntAPI($api_url);
$api->setVersion(RelyntApi::API_VERSION_2);

$isAuthorized = $api->login([
    'auth_type' => RelyntApi::AUTH_TYPE_ADMIN,
    'login' => $admin_login,
    'password' => $admin_password,
]);

if (!$isAuthorized) {
    exit("Authorization failed!\n");
}

$customersUrl = "admin/customers/customer";

$condition = [
    'main_attributes' => [
        'status' => 'active',
    ]
];

print "Count of active customers\n";
$result = $api->api_call_head($customersUrl . '?' . http_build_query($condition));
print "Result: ";
if ($result) {
    print "Ok!\n";
    $count = isset($api->response_headers[RelyntApi::HEADER_X_TOTAL_COUNT]) ? $api->response_headers[RelyntApi::HEADER_X_TOTAL_COUNT] : 0;
    print "Count: " . var_export($count, 1) . "\n";
} else {
    print "Fail! Error code: $api->response_code\n";
    print_r($api->response);
}
print "\n-------------------------------------------------\n";
