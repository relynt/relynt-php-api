<?php
/**
 * Relynt API v2.0 demo script
 * Author: Volodymyr Tsumanchuk (Relynt s.r.o.)
 * https://relyntv2rc.docs.apiary.io - API documentation
 */

include '../src/RelyntApi.php';

$api_url = 'http://relynt/'; // please set your Relynt URL

$key = "API_KEY"; // please set your key
$secret = "API_SECRET"; // please set your secret

// don't forget to add permissions to API Key, for changing locations.

$api = new RelyntAPI($api_url);
$api->setVersion(RelyntApi::API_VERSION_2);

$isAuthorized = $api->login([
    'auth_type' => RelyntApi::AUTH_TYPE_API_KEY,
    'key' => $key,
    'secret' => $secret,
]);

if (!$isAuthorized) {
    exit("Authorization failed!\n");
}

print "<pre>";

print "Authorization info: " . var_export($api->getAuthData(), 1) . "\n";

$locationsApiUrl = "admin/administration/locations";

print "Get count of locations\n";
$result = $api->api_call_head($locationsApiUrl);
print "Result: ";
if ($result) {
    print "Ok!\n";
    $countOfLocations = isset($api->response_headers[RelyntApi::HEADER_X_TOTAL_COUNT]) ? $api->response_headers[RelyntApi::HEADER_X_TOTAL_COUNT] : 0;
    print "Count of locations: " . print_r($countOfLocations, 1);
} else {
    print "Fail! Error code: $api->response_code\n";
    print_r($api->response);
}
print "\n-------------------------------------------------\n";

print "Get locations schema\n";
$result = $api->api_call_options($locationsApiUrl);
print "Result: ";
if ($result) {
    print "Ok!\n";
    print_r($api->response);
} else {
    print "Fail! Error code: $api->response_code\n";
    print_r($api->response);
}
print "\n-------------------------------------------------\n";

print "List locations\n";
$result = $api->api_call_get($locationsApiUrl);
print "Result: ";
if ($result) {
    print "Ok!\n";
    print_r($api->response);
} else {
    print "Fail! Error code: $api->response_code\n";
    print_r($api->response);
}
print "\n-------------------------------------------------\n";

print "Create location\n";
$result = $api->api_call_post($locationsApiUrl, [
    'name' => 'API test #' . rand()
]);

print "Result: ";
if ($result) {
    print "Ok!\n";
    print_r($api->response);
    $locationId = $api->response['id'];
} else {
    print "Fail! Error code: $api->response_code\n";
    print_r($api->response);
    $locationId = false;
}
print "\n-------------------------------------------------\n";

if ($locationId) {
    print "Retrieve location " . $locationId . "\n";
    $result = $api->api_call_get($locationsApiUrl, $locationId);
    print "Result: ";
    if ($result) {
        print "Ok!\n";
        print_r($api->response);
    } else {
        print "Fail! Error code: $api->response_code\n";
        print_r($api->response);
    }
    print "\n-------------------------------------------------\n";


    print "Change created location name\n";
    $result = $api->api_call_put($locationsApiUrl, $locationId, ['name' => 'NAME CHANGED #' . mt_rand()]);
    print "Result: ";
    if ($result) {
        print "Ok!\n";
        print_r($api->response);
    } else {
        print "Fail! Error code: $api->response_code\n";
        print_r($api->response);
    }
    print "\n-------------------------------------------------\n";

    print "Retrieve updated info\n";
    $result = $api->api_call_get($locationsApiUrl, $locationId);
    print "Result: ";
    if ($result) {
        print "Ok!\n";
        print_r($api->response);
    } else {
        print "Fail! Error code: $api->response_code\n";
        print_r($api->response);
    }
    print "\n-------------------------------------------------\n";

    print "Delete created location\n";
    $result = $api->api_call_delete($locationsApiUrl, $locationId);
    print "Result: ";
    if ($result) {
        print "Ok!\n";
        print_r($api->response);
    } else {
        print "Fail! Error code: $api->response_code\n";
        print_r($api->response);
    }
    print "\n-------------------------------------------------\n";
}
