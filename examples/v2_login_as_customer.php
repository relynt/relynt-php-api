<?php
/**
 * Relynt API v2.0 demo script
 * Author: Volodymyr Tsumanchuk (Relynt s.r.o.)
 * https://relyntv2rc.docs.apiary.io - API documentation
 */

include '../src/RelyntApi.php';

$api_url = 'http://relynt/'; // please set your Relynt URL

$customer_id = "CUSTOMER_ID"; // Relynt customer login
$customer_login = "CUSTOMER_LOGIN"; // Relynt customer login
$customer_password = "CUSTOMER_PASSWORD"; // Relynt customer password

$api = new RelyntAPI($api_url);
$api->setVersion(RelyntApi::API_VERSION_2);

$isAuthorized = $api->login([
    'auth_type' => RelyntApi::AUTH_TYPE_CUSTOMER,
    'login' => $customer_login,
    'password' => $customer_password,
]);

if (!$isAuthorized) {
    exit("Authorization failed!\n");
}

print "Authorization info: " . var_export($api->getAuthData(), 1) . "\n";

$customersApiUrl = "admin/customers/customer";

print "Get customer\n";
$result = $api->api_call_get($customersApiUrl, $customer_id);
print "Result: ";
if ($result) {
    print "Ok!\n";
    print_r($api->response);
} else {
    print "Fail! Error code: $api->response_code\n";
    print_r($api->response);
}
print "\n-------------------------------------------------\n";
